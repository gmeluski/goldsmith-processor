const AWS = require('aws-sdk');
const querystring = require('querystring');

const AWS_REGION = 'us-east-1';
const NAMES = ['goals', 'progress', 'meaning', 'happy', 'relationships', 'engaged']

// Set this to the region you upload the Lambda function to.
AWS.config.region = AWS_REGION;

exports.handler = function(evt, context, callback) {
  // Our raw request body will be in evt.body.
  const params = querystring.parse(evt.body);

  // Our field from the request.
  const my_field = params['my-field'];

  // Generate HTML.
  let html = `<!DOCTYPE html><p>You said: ` + my_field + `</p>`;
  for (let i = 0; i < NAMES.length; i++ ) {
    const category = NAMES[i];
    const output = params[category];
    html += `<p>` +  category + ` score: ` + output + `</p>`;
  }

  // Return HTML as the result.
  callback(null, html);
};

